import numpy as np
import pandas as pd
from tabulate import tabulate
import copy
import gym
from environment import Environment 
import datetime
import openpyxl
import os
import xlsxwriter
from stable_baselines3 import DQN
from stable_baselines3.dqn import MlpPolicy
from stable_baselines3.common.env_checker import check_env
from sklearn.model_selection import train_test_split


#To open and save the results i.e makespan,machine span,state span so on to a workbook 
if os.path.isfile('makespan.xlsx'):
    os.remove('makespan.xlsx')
    wb = openpyxl.Workbook()
    wb.save('makespan.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('makespan.xlsx')
    
if os.path.isfile('machinespan.xlsx'):
    os.remove('machinespan.xlsx')
    wb = openpyxl.Workbook()
    wb.save('machinespan.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('machinespan.xlsx')
    
if os.path.isfile('stagespan.xlsx'):
    os.remove('stagespan.xlsx')
    wb = openpyxl.Workbook()
    wb.save('stagespan.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('stagespan.xlsx')
    
if os.path.isfile('jobs.xlsx'):
    os.remove('jobs.xlsx')
    wb = openpyxl.Workbook()
    wb.save('jobs.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('jobs.xlsx')
    
if os.path.isfile('output.xlsx'):
    os.remove('output.xlsx')
    wb = openpyxl.Workbook()
    wb.save('output.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('output.xlsx')
    
if os.path.isfile('machines_output.xlsx'):
    os.remove('machines_output.xlsx')
    wb = openpyxl.Workbook()
    wb.save('machines_output.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('machines_output.xlsx')
    
if os.path.isfile('contingency.xlsx'):
    os.remove('contingency.xlsx')
    wb = openpyxl.Workbook()
    wb.save('contingency.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('contingency.xlsx')
    
tstart=datetime.datetime.now()
print(datetime.datetime.now())

# data_input = pd.read_csv("input.csv",sep=",")
data_machines = pd.read_csv("machines.csv",sep=",")
# ##print(data_input)
##print(data_machines)

xls = pd.ExcelFile('P1.xlsx')

# Now you can list all sheets in the file
xls.sheet_names


sheet_to_df_map = {}

#Run the loop till all sheets of workbook are executed 
for sheet_name in xls.sheet_names:
    readData = pd.read_excel("P1.xlsx", sheet_name=sheet_name)


    #readData=readData.sort_values(by=['Start Time '])
    
    #idList=readData['Order ID'].to_list()
    
    actionList=[]
    sheetData= readData[['Job ID','Priority'
                         ,'Product type','Processing time (SMD)','Processing time (AOI)','Processing time (SS)',
                         'Processing time (CC)','Deadline in (min)']].copy()
    sheetData.columns=['Jobs','priority','familyType','S-1','S-2','S-3','S-4','dueDate']
    
    sheetData=sheetData.fillna(0)
    
    train, test = train_test_split(sheetData, test_size=0.4)
   
    #sheetData = sheetData.head(20)
    #print(sheetData[sheetData['Jobs']==2])
    #sheetData.dropna(inplace=True)
    ##print(sheetData)
    
    
    
    iter_counter = 1
    env= gym.make("gym_stowkoski:stowkoski-env-v0",**{"input":sheetData,"machines":data_machines,"sheet":sheet_name})
    #env =DummyVecEnv([lambda: Environment(sheetData,data_machines,sheet_name)])
    #env= Environment(sheetData,data_machines,sheet_name)
    isComplete=env.isComplete
    counter=1
    #check_env(env, True)
    model = DQN(MlpPolicy, env, verbose=1,buffer_size=10000)
    model.learn(total_timesteps=160)
    env.setTest()
    obs = env.reset()
    #Run the loop till each row of the sheet gets processed
    while isComplete is not True:
        ###print(env.getAvailMachines(counter))
        ind=0
     
        avail_machines=env.getAvailMachines(counter)
    
        for i in avail_machines:
            possible_action=env.getAction(env.getActionTime(),avail_machines)
            env.setActionSpace(possible_action)
            some_action, _states = model.predict(obs, deterministic=True)
            #some_action=  env.getAction(env.getActionTime(),avail_machines)
            if some_action>=0:
                #print(some_action)
                #random_choice= some_action.sample()
                ###print("Random Choice :: "+str(random_choice))
                #actionList.append(some_action)
                #print(some_action)
                fnState,reward, isComplete,info = env.step(some_action) #some_action.iloc[0]
                ind+=1
                #if obs.all()==fnState.all():
                #    print('YEs')
                obs=fnState
                #env.setReward()
                #fixedActionMask=env.getFixedAction()
                #idList.pop(0)
                #print("job :: "+ str(tabulate(info['job'], headers='keys', tablefmt='psql')))
                #print("Contingency :: "+ str(tabulate(info['contingency'], headers='keys', tablefmt='psql')))
                #print("machine :: "+ str(tabulate(info['machine'], headers='keys', tablefmt='psql')))
                #print("Timetable :: "+ str(tabulate(info['timetable'], headers='keys', tablefmt='psql')))
                #str= 'fnstate'+str(ind)+'.csv'
                #fnState.to_csv(str)
                ##print("State :: "+ str(tabulate(fnState, headers='keys', tablefmt='psql')))
                 
        if len(avail_machines)<=0:
            env.checkComplete()
            isComplete=env.isComplete
            if isComplete is not True:
                counter+=1
                env.updateTime()
        else:
            counter+=1
            env.updateTime()
            isComplete=env.isComplete
        iter_counter += 1
        if iter_counter%10 == 0:
            #print(iter_counter)
            #timetable.to_csv("output.csv")
            print("Timetable :: "+ str(tabulate(info["timetable"], headers='keys', tablefmt='psql')))
     
    print("Game Over (::)")
    tend=datetime.datetime.now()
    print(datetime.datetime.now())
    
    print("total time :: "+str(tend-tstart))
    outputdata= pd.read_excel("output.xlsx",sheet_name=sheet_name)
    del outputdata['Unnamed: 0']
    
    out_time=outputdata.groupby('jobs').agg(['min', 'max'])['time']
    out_time['diff']=out_time['max']-out_time['min']
    
    
    
    stage_time=outputdata.groupby('stage').agg(['min', 'max'])['time']
    stage_time['diff']=stage_time['max']-stage_time['min']
    
    machine_time=outputdata.groupby('machine').agg(['min', 'max'])['time']
    machine_time['diff']=machine_time['max']-machine_time['min']
    
    
    with pd.ExcelWriter('makespan.xlsx',engine="openpyxl",mode='a') as writer1:  
        out_time.to_excel(writer1,sheet_name=sheet_name)
        writer1.close()
    with pd.ExcelWriter('stagespan.xlsx',engine="openpyxl",mode='a') as writer2:  
        stage_time.to_excel(writer2,sheet_name=sheet_name)
        writer2.close()
    with pd.ExcelWriter('machinespan.xlsx',engine="openpyxl",mode='a') as writer3:  
        machine_time.to_excel(writer3,sheet_name=sheet_name)
        writer3.close()
    

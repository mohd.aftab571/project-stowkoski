import numpy as np
import pandas as pd
from tabulate import tabulate
import copy
import gym
from prompt_toolkit import input
from pandas._libs import index
import openpyxl

import seaborn as sns


class Environment(gym.Env):
    #Calls the reset method
    def __init__(self, input, machines,sheet):
        self.actionList=[]
        self.input=input
        self.sheet_name=sheet
        self.machines=machines
        self.reset()
        
    #Initializes the input variables,setup times and  associates the jobs and machines with the contingency table
    def reset(self):
        
        self.time=0
        self.reward=0
        self.isComplete=False
        self.majorTime=65
        self.minorTimne=20
        self.globalSetupTime=0
        
        self.Jobs_state= pd.DataFrame(columns=set(self.machines['stage']))
        #Jobs_state= pd.DataFrame(columns=['Jobs','isStarted','isEnded','isInS1','isInS2'])
        self.lastStage=self.machines['stage'].max()
        self.Jobs_state['Jobs']=self.input['Jobs']
        self.Jobs_state['isStarted']=0
        self.Jobs_state['isEnded']=0
        self.Jobs_state=self.Jobs_state.replace(np.nan,0)
        ####print(self.Jobs_state)
        self.machine_state= pd.DataFrame(columns=list(self.machines['machine']))
        self.machine_state['Jobs']=self.input['Jobs']
        self.machine_state=self.machine_state.replace(np.nan,0)
        ####print(self.machine_state)
        outList= [ (i, j)for i in self.input['Jobs'] for j in self.machines['machine'] ]
        self.inner_table = pd.DataFrame(data=outList, columns=['Jobs','machine'])
        self.contingency_table=self.inner_table.merge(self.machines,how='inner',on='machine')
        #self.contingency_table = self.contingency_table.merge()
        self.contingency_table['canAssign']=1
        self.contingency_table['eligibleFor']="S-1"#self.contingency_table['stage']
        ####print(self.contingency_table)
        self.timeTable=pd.DataFrame(columns=['time','reward','jobs','assignment','expectedTime','stage','machine', 
                                             'family_type', 'setup_time'])
        self.timeQueue=[]
        self.contingency_table['eligibleFor']
        mystate= self.createState()
    
        #self.action_space = gym.spaces.Box(low=0.0, high=160, shape=self.contingency_table.shape, dtype=np.float32)
        self.observation_space = gym.spaces.Box(low=0.0, high=mystate['dueDate'].max(), shape=mystate.shape, dtype=np.float32)
        #self.contingency_table.index.to_numpy()
        actions = self.getAction(self.time, self.getAvailMachines(1))
        self.action_space = gym.spaces.Discrete(actions.shape[0])
        #self.observation_space = gym.spaces.Dict(mystate.to_dict())
        self.contCopy= self.contingency_table.copy(True)
        #data=mystate[['Jobs', 'isStarted', 'isEnded','priority', 'familyType', 'S-1_y', 'S-2_y', 'S-3_y', 'S-4_y', 'dueDate','M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11','M12', 'M13', 'M14', 'M15', 'M16', 'M17']]
        #data=data.dropna(subset=['Jobs'])
        #data = data.style.background_gradient()
        #dfi.export(data, 'df_styled.png')
        return mystate.to_numpy(np.float32)
    
    #Lets the agent know the available eligible jobs that can be assigned onto the machines
    def enforceEligibleJobs(self,availableActions):
        unassigned_df=self.getUnassigned()
        ###print(availableActions)
        df= pd.DataFrame(availableActions.groupby('Jobs')['stage'].min())
        df_unassgned= pd.DataFrame(unassigned_df.groupby('jobs')['stage'].max())
        df['flag']=0
        for i, item in df.iterrows():
            for j, inner_item in df_unassgned.iterrows():
                if (i==j) & (item['stage']> inner_item['stage']):
                    if  (int(item['stage'].split('-')[1])-int(inner_item['stage'].split('-')[1])==1):
                        df.at[i,'flag']=1
        df['flag']=np.where((df['flag']==0) &(df['stage']=='S-1'),1,df['flag'])    
        df=df[df['flag']==1] 
                    
        ###print(df)
        final_df_for_merge=pd.DataFrame(df).reset_index()
        ###print(final_df_for_merge)
        try:
            final_df=availableActions.merge(final_df_for_merge,on=['Jobs','stage'],how='inner')
            return final_df
        except:
            ##print(final_df_for_merge)
            ##print(availableActions)
            return pd.DataFrame()
            
        #df_sort=df.sortby('stage')
        ###print(final_df)
        
        #unique_avaliable_actions= availableActions.drop_duplicates('Jobs')
        #for i, row in unique_avaliable_actions.iterrows():
            
        
    def getFixedAction(self):
        return self.contingency_table    
        
    #Action from agent is used to update the job , machine state and contingency table  
    def getAction(self,time,avail_machines):
        ####print(time)
        if self.time==0:
            return self.contingency_table[(self.contingency_table['stage']=='S-1') & (self.contingency_table['canAssign']==1)]
        else:
            self.unassign_next_state()
            df=self.contingency_table[(self.contingency_table['canAssign']==1) & (self.contingency_table['machine'].isin(avail_machines))]
            return self.enforceEligibleJobs(df)
    
    def setActionTime(self,time):
        self.time=time

    def setReward(self):
        self.reward=0
        
    def getActionTime(self):
        return self.time
    
    #Positive reward (due date - processed time ) or negative reward of square of extra elapsed time is returned 
    def calculateReward(self,job,stage):
        ind = self.input.index[self.input['Jobs']==job[0]].tolist()
        proc_time= self.input.at[ind[0],'dueDate']
        
        if stage[0]=='S-4':
            new_reward=proc_time-self.time
            self.reward=self.reward+new_reward
        else:
            self.reward=self.reward
            
        if self.reward<0:
            self.reward=self.reward*self.reward*-1
            
    #Checks whether all the jobs have been processed and terminates the episode
    def checkComplete(self):
        if len(self.timeQueue)<=0:
            isEnded=self.Jobs_state[self.Jobs_state['isEnded']==0]
            if isEnded.shape[0]<=0:
                self.isComplete=True
                with pd.ExcelWriter('output.xlsx',engine="openpyxl",mode='a') as writer:  
                    self.timeTable.to_excel(writer,sheet_name=self.sheet_name)
                    writer.close()
                with pd.ExcelWriter('machines_output.xlsx',engine="openpyxl",mode='a') as writer:
                    self.machine_state.to_excel(writer,sheet_name=self.sheet_name)
                    writer.close()
                with pd.ExcelWriter('contingency.xlsx',engine="openpyxl",mode='a') as writer:
                    self.contingency_table.to_excel(writer,sheet_name=self.sheet_name)
                    writer.close()
                with pd.ExcelWriter('jobs.xlsx',engine="openpyxl",mode='a') as writer:
                    self.Jobs_state.to_excel(writer,sheet_name=self.sheet_name)
                    writer.close()
        else:
            self.isComplete=False
    
    #State here constitutes job id,processing time per stage ,due date, whether job has started/ended and on which machine it is executing 
    def createState(self):  
 
        '''  if self.timeTable['time'][len(self.timeTable['time'])-1]==271:
            print("Time:"+str(self.timeTable['time']))'''
        state_1= self.Jobs_state.merge(self.input, on="Jobs", how='inner')
        state_1= state_1.sort_values(by='Jobs',ascending=True)
        self.machine_state= self.machine_state.sort_values(by='Jobs')
       
        col_list=self.machine_state.columns
        state_1.reset_index(drop=True,inplace=True)
        self.machine_state.reset_index(drop=True,inplace=True)
        for i in col_list:
            state_1[i]=self.machine_state[i]
        return state_1
    
    #Definies the next action that needs to be performed within the environment
    def step(self,action):
        #if typ=='Assign':
            ####print("here:: "+str(action.iloc[0]))
        #new_state=self.unassign_next_state()
        #self.checkComplete()
        #reward =self.rewardactionList
        #self.actionList.append(action)
        #print(str(self.actionList))s
        #self.actionList.append(action)
        action1= pd.DataFrame(self.contCopy.loc[action]).T
        action2= self.contCopy.loc[action]
        
        job,contingency,machine=self.assign_next_state(action1)
        #print(machine[machine['Jobs']==list(action1['Jobs'])[0]])
        #print(job[job['Jobs']==list(action1['Jobs'])[0]])
        #print(contingency[contingency['Jobs']==list(action1['Jobs'])[0]])
        reward= self.reward
        self.updateTimeTable(self.time,reward,action2,"A")
        self.updateQueue(action2)
        self.checkComplete()
        #self.updateTime()
        self.contCopy.reset_index(drop=True,inplace=True)
        #self.contCopy.index+=1
        #print(self.contCopy.index)
        fnstate=self.createState()
        
        
        #metadata=dict()
        #metadata['action_mask']=self.contingency_table
     
        
        #data=fnstate[['Jobs', 'isStarted', 'isEnded','priority', 'familyType', 'S-1_y', 'S-2_y', 'S-3_y', 'S-4_y', 'dueDate','M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11','M12', 'M13', 'M14', 'M15', 'M16', 'M17']]
        #data=data.dropna(subset=['Jobs'])
        #data = data.style.background_gradient()
        #dfi.export(data, 'df_styled.png')
        info={"job":job,"contingency":contingency,"machine":machine,"timetable":self.timeTable}
        return fnstate.to_numpy(np.float32),reward,self.isComplete,info
    
    def setData(self,avail):
        actions= self.getAction(self.time, avail)
        self.contCopy=actions
        self.action_space = gym.spaces.Discrete(actions.shape[0])
       
    #Assigns the next job onto the machines and updates the Jobs,Machine and contingency table
    def assign_next_state(self,action):
        #update dataframes
        self.update(self.Jobs_state,action,"A","J")
        self.update(self.machine_state,action,"A","M")
        self.update(self.contingency_table,action,"A","C")
     
        return self.Jobs_state,self.contingency_table,self.machine_state
    
    #Along with the assign action received from the agent ,unassigning of the completed jobs from machines is executed 
    def unassign_next_state(self):   
        #update dataframes
        df= self.jobsToBeUnassigned()
        for i, row in df.iterrows():
            row_df= pd.DataFrame(row).T
            self.update_unassign(self.Jobs_state,row_df,"U","J")
            self.update_unassign(self.machine_state,row_df,"U","M")
            self.update_unassign(self.contingency_table,row_df,"U","C")
            self.updateTimeTable(self.time,self.reward,row,"U")
            self.updateQueueUnassign(row)
        return self.Jobs_state,self.contingency_table,self.machine_state
    
    #Unassignment of job from particular machine based on latest value popped from time queue and returns calculated reward 
    def update_unassign(self,table,action,typ,tab_typ):
        if tab_typ=="J":
            stage_number = list(action['stage'])
            Jobs_number = list(action['Jobs'])
            ind=table.index[table['Jobs'] == Jobs_number[0]].tolist()
            table.at[ind[0],stage_number[0]]= 1 if typ=="A" else 0
            if Jobs_number[0]==3:
                print(Jobs_number)
            if self.lastStage==stage_number[0]:
                table.at[ind[0],'isEnded']= 1
            self.Jobs_state=table
        if tab_typ=="M":
            machine_number = list(action['machine'])
            Jobs_number = list(action['Jobs'])
            ind=table.index[(table['Jobs'] == Jobs_number[0])].tolist()
            column_names=table.columns.values.tolist()
            column_names.remove('Jobs')
            for i in column_names:
                table.at[ind[0],i]= 0
            table.at[ind[0],machine_number[0]]= 1 if typ=="A" else 0
            self.machine_state=table
        if tab_typ=="C":
            stage_number = list(action['stage'])
            stage_list=stage_number[0].split("-")
            nxt_stage='S-'+str(int(stage_list[1])+1)
            machine_number = list(action['machine'])
            Jobs_number = list(action['Jobs'])
            ind=table.index[(table['Jobs'] == Jobs_number[0]) & (table['stage'] == stage_number[0])].tolist()
            #assign all the rows which have a machine that has been assigned already to zero
            #table.loc[table['machine'].isin(machine_number),table["canAssign"]]=0 if typ=='A' else 1
            assign_jobs,assign_machines= self.getAssigned()
            table=table.drop(ind,axis=0)
            table['canAssign'] = np.where(( (table['Jobs'] == Jobs_number[0]) & (table['canAssign'] == 0)),1,table['canAssign'])
            table['canAssign'] = np.where(( (table['machine'] == machine_number[0]) & (~table['Jobs'].isin(assign_jobs)) & (table['canAssign'] == 0)),1,table['canAssign'])
            #del_ind=table.index[(table['Jobs'] == Jobs_number[0]) & (table['stage'] == stage_number[0]) & (table['canAssign'] == 1)].tolist()
            #self.contingency_table=self.contingency_table.drop(del_ind,axis=0)
            #if typ=="U":
            #table.at[ind[0],'eligibleFor']= nxt_stage
            
            self.contingency_table=table
            if self.contingency_table.shape[0]==1:
                self.contingency_table['canAssign']=1
     
            df_machines=self.machine_state[self.machine_state['Jobs'].isin(self.contingency_table["Jobs"][self.contingency_table["machine"]==machine_number[0]].tolist())] 
            job_names=df_machines[['Jobs']]
            job_names['free']=df_machines.loc[:,df_machines.columns!='Jobs'].any(axis='columns')
          
            job_names=job_names[job_names['free']==False]
            del job_names['free']
            job_names=job_names['Jobs'].tolist()
            
            self.contingency_table['canAssign']=np.where(( (self.contingency_table['Jobs'].isin(job_names)) & (self.contingency_table['machine'] == machine_number[0])),1,self.contingency_table['canAssign'])
            
            #if len(self.contingency_table['canAssign'].unique()) ==1:
                #print(self.contingency_table)
            
            #self.contingency_table["canAssign"]= np.where((self.contingency_table["machine"]==machine_number[0]) & (self.contingency_table["eligibleFor"]==stage_number[0]),1,self.contingency_table["canAssign"]) 
            self.calculateReward(Jobs_number, stage_number)
      
    #Updates the tables :job ,machine ,contingency to indicate the action executed       
    def update(self,table,action,typ,tab_typ):
        #logic
        if tab_typ=="J":
            stage_number = list(action['stage'])
            Jobs_number = list(action['Jobs'])
            ind=table.index[table['Jobs'] == Jobs_number[0]].tolist()
            table.at[ind[0],stage_number[0]]= 1 if typ=="A" else 0
        if tab_typ=="M":
            machine_number = list(action['machine'])
            Jobs_number = list(action['Jobs'])
            ind=table.index[(table['Jobs'] == Jobs_number[0])].tolist()
            column_names=table.columns.values.tolist()
            column_names.remove('Jobs')
            for i in column_names:
                table.at[ind[0],i]= 0
            table.at[ind[0],machine_number[0]]= 1 if typ=="A" else 0
        if tab_typ=="C":
            stage_number = list(action['stage'])
            stage_list=stage_number[0].split("-")
            nxt_stage='S-'+str(int(stage_list[1])+1)
            machine_number = list(action['machine'])
            Jobs_number = list(action['Jobs'])
            ind=table.index[(table['Jobs'] == Jobs_number[0]) & (table['machine'] == machine_number[0])].tolist()
            #assign all the rows which have a machine that has been assigned already to zero
            #table.loc[table['machine'].isin(machine_number),table["canAssign"]]=0 if typ=='A' else 1
            table['canAssign'] = np.where(( (table['Jobs'] == Jobs_number[0]) & (table['canAssign'] == 1)),0,table['canAssign'])
            table['canAssign'] = np.where(((table['machine'] == machine_number[0]) & (table['canAssign'] == 1)),0,table['canAssign'])
            del_ind=table.index[(table['Jobs'] == Jobs_number[0]) & (table['stage'] == stage_number[0]) & (table['canAssign'] == 1)].tolist()
            self.contingency_table=self.contingency_table.drop(del_ind,axis=0)
            
            if typ=="U":
                table.at[ind[0],'eligibleFor']= nxt_stage
                self.calculateReward(Jobs_number, stage_number)
    
    #Gets a list of free machines on which no jobs are running       
    def getAvailMachines(self,isFirst):    
        if isFirst==1:
            possible_list=self.contingency_table[(self.contingency_table['canAssign']==1) & (self.contingency_table['stage']=='S-1') & (self.contingency_table['eligibleFor']=='S-1')]
        else:
            self.unassign_next_state()
            possible_list=self.contingency_table[(self.contingency_table['canAssign']==1)]
        possible_machine= possible_list['machine'].tolist()
        available_free_machine=[]
        for x in set(possible_machine):
            if (self.machine_state[x]==0).all():
                available_free_machine.append(x)
        return available_free_machine
    
    
    #Setup times of 60 or 20 min are assigned on Stage -1 and 4
    def calculate_setup_time(self,timeTable, machine, stage,family):
        if timeTable.shape[0]>0:
            matching_rows = timeTable[(timeTable['machine'] == machine) & (timeTable['stage'] == stage) ]
            if matching_rows.shape[0]>0:
                matching_rows = matching_rows.tail(1)
                lastFamily=matching_rows['family_type'].tolist()[0]
                if lastFamily==family:
                    if stage=='S-1':
                        return 20
                    if stage=='S-2':
                        return 15
                    if stage=='S-3':
                        return 25
                    if stage=='S-4':
                        return 15
                else:
                    if (stage=='S-1' or stage=='S-4'):
                        if stage=='S-1':
                            return 65
                        if stage=='S-4':
                            return 120
                    else:
                        if stage=='S-1':
                            return 20
                        if stage=='S-2':
                            return 15
                        if stage=='S-3':
                            return 25
                        if stage=='S-4':
                            return 15
               
                        
            else:
                if (stage=='S-1' or stage=='S-4'):
                    if stage=='S-1':
                        return 65
                    if stage=='S-4':
                        return 120
                else:
                    if stage=='S-1':
                        return 20
                    if stage=='S-2':
                        return 15
                    if stage=='S-3':
                        return 25
                    if stage=='S-4':
                        return 15
                
        else :
            return self.majorTime
        
        
        
    
    
    #Time table is updated to reflect current time ,reward, job,machine,family type on each assignment and unassignment .
    def updateTimeTable(self,time,reward,action,assign):
        self.globalSetupTime=0
        family_type = self.input['familyType'][self.input['Jobs'] == action['Jobs']].tolist()[0]
        if assign=='A':
            setupTime=self.calculate_setup_time(self.timeTable, action['machine'], action['stage'],family_type)
        else:
            setupTime=0
        self.globalSetupTime=setupTime
        self.timeTable=self.timeTable.append({'time': time, 'reward': reward, 'jobs': action['Jobs'],'assignment': assign,'machine':action['machine'],'family_type':family_type,'setup_time':setupTime}, ignore_index=True) 
        ####print(self.timeTable)
    #def updateTime(self):
       
    #Time queue is updated to reflect unassignment of individual jobs
    def updateQueueUnassign(self,action):
        job= action['Jobs']
        ind=self.input.index[self.input['Jobs']==job].tolist()
        stage= action['stage']
        proc_time_per_stage=self.input.at[ind[0], stage]
        fin_time=self.time+proc_time_per_stage
        #print("self time::"+str(self.time))
        self.timeQueue.pop(self.timeQueue.index(self.time))
        self.timeTable.at[self.timeTable.shape[0]-1,'expectedTime']=fin_time+self.globalSetupTime
        self.timeTable.at[self.timeTable.shape[0]-1,'stage']=stage
      
        ###print(self.timeQueue)
     
    def updateQueue(self,action):
        job= action['Jobs']
        ind=self.input.index[self.input['Jobs']==job].tolist()
        stage= action['stage']
        proc_time_per_stage=self.input.at[ind[0], stage]
        fin_time=self.time+proc_time_per_stage
        self.timeQueue.append(fin_time+self.globalSetupTime)
        self.timeTable.at[self.timeTable.shape[0]-1,'expectedTime']=fin_time+self.globalSetupTime
        self.timeTable.at[self.timeTable.shape[0]-1,'stage']=stage
        
        ###print(self.Jobs_state)
        ###print(self.timeQueue)
       ###print(self.timeTable.head(20))
       
    #Lets the environment know the jobs which has completed and needs to be unassigned 
    def jobsToBeUnassigned(self):
        if self.time>0:
            assignment_table_data_unassign=self.timeTable[(self.timeTable['assignment']=='A') & (self.timeTable['expectedTime']==self.time) ]
            #return contingency table rows for unassigning
            stage_list=assignment_table_data_unassign['stage'].tolist()
            job_list=assignment_table_data_unassign['jobs'].tolist()
            machine_list=assignment_table_data_unassign['machine'].tolist()
            
            df=assignment_table_data_unassign.merge(self.contingency_table,left_on=['jobs','stage','machine']
                                                 ,right_on=['Jobs','stage','machine'],how="inner")
            
            
            #df=self.contingency_table.loc[(self.contingency_table['Jobs'].isin(job_list)) & (self.contingency_table['machine'].isin(machine_list)) & (self.contingency_table['stage'].isin(stage_list))]
            
            return df
        else:
            return pd.DataFrame()
    
    def updateTime(self):
        if len(self.timeQueue)>0:
            self.time= min(self.timeQueue)
        else:
            self.checkComplete()
        ##print("time queue :: "+str(self.timeQueue))
    
    #Returns the list of all jobs,machines  that are given assign action
    def getAssigned(self):
        assigned=self.timeTable[self.timeTable['assignment']=='A']
        unassigned=self.timeTable[self.timeTable['assignment']=='U']
        df=assigned[~assigned.isin(unassigned)].dropna()
        
        return df['jobs'].tolist(),df['machine'].tolist()
    
    #Returns the list of all jobs,machines  that are given unassign action
    def getUnassigned(self):
        assigned=self.timeTable[self.timeTable['assignment']=='A']
        unassigned=self.timeTable[self.timeTable['assignment']=='U']
        #df=assigned[~assigned.isin(unassigned)].dropna()
        
        return unassigned
    
  
         
import numpy as np
import pandas as pd
from tabulate import tabulate
import copy
import gym
from environment import Environment 
import datetime
import openpyxl
import os
import xlsxwriter

if os.path.isfile('makespan.xlsx'):
    os.remove('makespan.xlsx')
    wb = openpyxl.Workbook()
    wb.save('makespan.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('makespan.xlsx')
    
if os.path.isfile('machinespan.xlsx'):
    os.remove('machinespan.xlsx')
    wb = openpyxl.Workbook()
    wb.save('machinespan.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('machinespan.xlsx')
    
if os.path.isfile('stagespan.xlsx'):
    os.remove('stagespan.xlsx')
    wb = openpyxl.Workbook()
    wb.save('stagespan.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('stagespan.xlsx')
    
if os.path.isfile('jobs.xlsx'):
    os.remove('jobs.xlsx')
    wb = openpyxl.Workbook()
    wb.save('jobs.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('jobs.xlsx')
    
if os.path.isfile('output.xlsx'):
    os.remove('output.xlsx')
    wb = openpyxl.Workbook()
    wb.save('output.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('output.xlsx')
    
if os.path.isfile('machines_output.xlsx'):
    os.remove('machines_output.xlsx')
    wb = openpyxl.Workbook()
    wb.save('machines_output.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('machines_output.xlsx')
    
if os.path.isfile('contingency.xlsx'):
    os.remove('contingency.xlsx')
    wb = openpyxl.Workbook()
    wb.save('contingency.xlsx')
else:
    wb = openpyxl.Workbook()
    wb.save('contingency.xlsx')
    
tstart=datetime.datetime.now()
print(datetime.datetime.now())

# data_input = pd.read_csv("input.csv",sep=",")
data_machines = pd.read_csv("machines.csv",sep=",")
# ##print(data_input)
##print(data_machines)

xls = pd.ExcelFile('input2.xlsx')

# Now you can list all sheets in the file
xls.sheet_names
#

sheet_to_df_map = {}
for sheet_name in xls.sheet_names:
    readData = pd.read_excel("input2.xlsx", sheet_name=sheet_name)


    readData=readData.sort_values(by=['Start Time '])
    
    print(readData['Order ID'].head(20))
    
    
    sheetData= readData[['Order ID','Priority'
                         ,'Family Type','Working Time SMD','Working Time AOI','Working Time SS',
                         'Working Time Painting']].copy()
    sheetData.columns=['Jobs','priority','familyType','S-1','S-2','S-3','S-4']
    
    sheetData=sheetData.fillna(0)
    #Aftab
    #sheetData = sheetData.head(20)
    #print(sheetData[sheetData['Jobs']==2])
    #sheetData.dropna(inplace=True)
    ##print(sheetData)
    
    
    
    iter_counter = 1
    
    env =Environment(sheetData,data_machines,sheet_name)
    isComplete=env.isComplete
    counter=1
    while isComplete is not True:
        ###print(env.getAvailMachines(counter))
        ind=0
     
        avail_machines=env.getAvailMachines(counter)
    
        for i in avail_machines:
            
            some_action=  env.getAction(env.getActionTime(),avail_machines)
            if some_action.shape[0]>0:
                random_choice= some_action.sample()
                ###print("Random Choice :: "+str(random_choice))
                job,contingency,machine,reward, isComplete,timetable=env.step(some_action.iloc[0],"Assign") 
                ind+=1
                ###print("job :: "+ str(tabulate(job, headers='keys', tablefmt='psql')))
                ##print("Contingency :: "+ str(tabulate(contingency, headers='keys', tablefmt='psql')))
                ##print("machine :: "+ str(tabulate(machine, headers='keys', tablefmt='psql')))
                ##print("Timetable :: "+ str(tabulate(timetable, headers='keys', tablefmt='psql')))
                 
        if len(avail_machines)<=0:
            env.checkComplete()
            isComplete=env.isComplete
            if isComplete is not True:
                counter+=1
                env.updateTime()
        else:
            counter+=1
            env.updateTime()
            isComplete=env.isComplete
        iter_counter += 1
        if iter_counter%50 == 0:
            print(iter_counter)
            #timetable.to_csv("output.csv")
            print("Timetable :: "+ str(tabulate(timetable, headers='keys', tablefmt='psql')))
     
    print("Game Over (::)")
    tend=datetime.datetime.now()
    print(datetime.datetime.now())
    
    print("total time :: "+str(tend-tstart))
    outputdata= pd.read_excel("output.xlsx",sheet_name=sheet_name)
    del outputdata['Unnamed: 0']
    
    out_time=outputdata.groupby('jobs').agg(['min', 'max'])['time']
    out_time['diff']=out_time['max']-out_time['min']
    
    
    
    stage_time=outputdata.groupby('stage').agg(['min', 'max'])['time']
    stage_time['diff']=stage_time['max']-stage_time['min']
    
    machine_time=outputdata.groupby('machine').agg(['min', 'max'])['time']
    machine_time['diff']=machine_time['max']-machine_time['min']
    
    
    with pd.ExcelWriter('makespan.xlsx',engine="openpyxl",mode='a') as writer1:  
        out_time.to_excel(writer1,sheet_name=sheet_name)
        writer1.close()
    with pd.ExcelWriter('stagespan.xlsx',engine="openpyxl",mode='a') as writer2:  
        stage_time.to_excel(writer2,sheet_name=sheet_name)
        writer2.close()
    with pd.ExcelWriter('machinespan.xlsx',engine="openpyxl",mode='a') as writer3:  
        machine_time.to_excel(writer3,sheet_name=sheet_name)
        writer3.close()
    

from gym.envs.registration import register

register(
    id='stowkoski-main',
    entry_point='main',
)
register(
    id='stowkoski-env-v0',
    entry_point='FooExtraHardEnv',
)